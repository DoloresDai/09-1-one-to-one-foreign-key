package com.twuc.webApp.domain.oneToOne;

import com.twuc.webApp.domain.ClosureValue;
import com.twuc.webApp.domain.JpaTestBase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

class UserAndProfileTest extends JpaTestBase {
    @Autowired
    private UserEntityRepository userEntityRepository;

    @Autowired
    private UserProfileEntityRepository userProfileEntityRepository;

    private ClosureValue<Long> userValue = new ClosureValue<>();
    private ClosureValue<Long> profileValue = new ClosureValue<>();
    private UserEntity userEntity;
    private UserProfileEntity profileEntity;

    @BeforeEach
    void setUp() {
        userEntity = new UserEntity();
        profileEntity = new UserProfileEntity("first","last");
    }

    @Test
    void should_save_user_and_profile() {
        // TODO
        //
        // 请书写一个测试实现如下的功能：
        //
        // Given 一个 UserEntity 和一个 UserProfileEntity。它们并没有持久化。
        // When 持久化 UserEntity
        // Then UserProfileEntity 也一同被持久化了。
        //
        // <--start--
        flushAndClear(em -> {
            userEntity.setUserProfileEntity(profileEntity);
            userEntityRepository.save(userEntity);
            userEntityRepository.flush();
            userValue.setValue(userEntity.getId());
            profileValue.setValue(profileEntity.getId());
        });


        flushAndClear(em -> {
            UserProfileEntity userProfileEntity = userProfileEntityRepository.findById(profileValue.getValue()).orElseThrow(RuntimeException::new);
            assertEquals(profileEntity.getId(), userProfileEntity.getId());
        });
        // --end->
    }

    @Test
    void should_remove_parent_and_child() {
        // TODO
        //
        // 请书写一个测试：
        //
        // Given 一个持久化了的 UserEntity 和 UserProfileEntity
        // When 删除 UserEntity
        // Then UserProfileEntity 也被删除了
        //
        // <--start-
        flushAndClear(em->{
            userEntity.setUserProfileEntity(profileEntity);
            userEntityRepository.save(userEntity);
            userEntityRepository.flush();

            userProfileEntityRepository.save(profileEntity);
        });

        flushAndClear(em->{
            userEntityRepository.delete(userEntity);
        });

        flushAndClear(em->{
            assertEquals(0,userProfileEntityRepository.findAll().size());
        });
        // --end->
    }
}
